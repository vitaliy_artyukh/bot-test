<?php

namespace App\Exceptions;

class RestException extends \Exception
{
    protected $error = 412;
    protected $code = 1412;
    protected $message = 'Rest exception';
    protected $data = [];

    public function __construct(array $params = [], $previous = null) {
        if(array_key_exists(ERR, $params)) $this->error = $params[ERR];
        if(array_key_exists(CODE, $params)) $this->code = $params[CODE];
        if(array_key_exists(MSG, $params)) $this->message = $params[MSG];
        if(array_key_exists(DATA, $params)) $this->data = $params[DATA];
        parent::__construct($this->message, $this->code, $previous);
    }

    public function getExceptionError() {
        return $this->error;
    }

    public function getExceptionCode() {
        return $this->code;
    }

    public function getExceptionMessage() {
        return $this->message;
    }

    public function getExceptionData() {
        return $this->data;
    }
}
