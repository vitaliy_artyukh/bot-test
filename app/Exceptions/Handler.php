<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception  $exception
     * @return void
     */

    public function report(Exception $exception) {
        parent::report($exception);
    }

    /**
    * Render an exception into an HTTP response.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  \Exception  $exception
    * @return \Illuminate\Http\Response
    */

    public function render($request, Exception $exception) {
        if(config("app.debug")) {
            $response['debug'][CODE] = $exception->getCode();
            $response['debug'][MSG] = $exception->getMessage();
            $response['debug'][FILE] = $exception->getFile();
            $response['debug'][LINE] = $exception->getLine();
            $response['debug']['exception'] = get_class($exception);
            // for SQL Exceptions
            if($exception instanceof \PDOException) {
                $response['debug'][SQL] = $exception->getSql(); // исходный sql запрос
                $response['debug']['bindings'] = $exception->getBindings(); // параметры запроса
            }
            if(config('app.log_level') == 'debug') {
                $response['debug']['trace'] = array_map(function ($trace) {
                    return array_only($trace, ['file', 'line', 'function', 'class']);
                }, $exception->getTrace());
            }
        }
        // если запрос типа ajax вернуть REST ответ
        if($request->isXmlHttpRequest() || $request->expectsJson()) {
            $response[ERR] = 400;
            $response[MSG] = "Server error";

            // Роут не найден
            if ($exception instanceof \Symfony\Component\HttpKernel\Exception\NotFoundHttpException) {
                $response[ERR] = 404;
                $response[CODE] = 1404;
                $response[MSG] = "API route [{$request->path()}] not found";
            }
            // Ошибка запрещения доступа
            if ($exception instanceof \Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException) {
                $response[ERR] = 403;
                $response[MSG] = "Access denied";
            }
            // Синтаксическая ошибка
            if ($exception instanceof \Symfony\Component\Debug\Exception\FatalThrowableError) {
                $response[ERR] = 500;
                $response[MSG] = "Fatal error";
            }
            // SQL Exceptions
            if($exception instanceof \Illuminate\Database\QueryException) {
                $response[ERR] = 400;
                $response[MSG] = "SQL error";
            }
            // Ошибки валидации
            if($exception instanceof \Illuminate\Validation\ValidationException) {
                $response[ERR] = 422;
                // $response[DATA] = $exception->getResponse();
                $response[MSG] = $exception->getMessage();
                // return $exception->getResponse();
            }
            // ошибки API
            if ($exception instanceof RestException) {
                $response[ERR] = $exception->getExceptionError();
                $response[CODE] = $exception->getExceptionCode();
                $response[MSG] = $exception->getExceptionMessage();
                if($exception->getExceptionData()) $response[DATA] = $exception->getExceptionData();
            }
            return response()->json($response, $response[ERR]);
        }
        return parent::render($request, $exception);
    }
}
