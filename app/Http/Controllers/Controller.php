<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Symfony\Component\HttpFoundation\Response;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /**
    * Returns REST response
    *
    * @param array|integer $error Error Code or array of params
    * @param array|null $params Array of additional params
    *
    * @return REST array
    */

    protected function response($params = array()) {
        $response = array_only($params, [ERR, CODE, MSG, DATA]);
        if (!isset($response[ERR])) $response[ERR] = Response::HTTP_OK;
        // http_response_code($response[ERR]);
        // Response::setStatusCode($response[ERR]);
        return $response;
    }
}
