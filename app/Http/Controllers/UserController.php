<?php

namespace App\Http\Controllers;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Pagination\Paginator;

class UserController extends Controller
{
    public function viewUserList() {
        return view("face.user.user-list");
    }

    public function getUserList(Request $request) {
        $items = User::paginate(20);
        return $this->response([CODE => 1200, DATA => $items]);
    }

    public function postUserProfile(Request $request) {
        $this->validate($request, [
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required|email'
        ]);
        $allow = ['id', 'first_name', 'last_name', 'username', 'email', 'phone'];
        if(!$request->id) {
            $allow[] = 'password';
            $allow[] = 'avatar';
        }
        $data = array_only($request->all(), $allow);
        if(!$request->id) {
            $data['avatar'] = "https://lorempixel.com/200/200/?" . rand(11111, 99999);
        }
        $post = User::updateOrCreate(['id' => $request->id ?: null], $data);
        if($post->wasRecentlyCreated) return $this->response([ERR => 201, CODE => 1201, DATA => $post->getAttributes(), MSG => 'User success create']);
        return $this->response([ERR => 204, CODE => 1204, MSG => 'User success update']);
    }
}
