<?php

namespace App\Http\Middleware;

use Closure;
use App\AclResource;
use App\Exceptions\RestException;
use Illuminate\Routing\Route;
use Illuminate\Support\Facades\DB;

class Permission
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next) {
        // Get the current route.
        $route = $request->route();
        // наличие алиаса у роута
        // if(!$route->getName()) throw new RestException([ERR => 412, CODE => 1412, MSG => "Route [{$request->path()}] has not alias name. Add it!"]);
        // if(!$this->checkPermission('api::' . $route->getName())) {
        //     throw new RestException([ERR => 403, CODE => 1403, MSG => "Access denied"]);
        // }
        // logger()->info([$route->getName(), $request->path()]);
        return $next($request);
    }

    private function getUserPermission() {
        $items = Db::table('acl_resource AS resource')->select(DB::raw('CONCAT(resource.type, "::", resource.path) AS `path`'))
            ->leftJoin('acl_role_permission AS role_permission', 'role_permission.resource_id', '=', 'resource.id')
            ->leftJoin('acl_role AS role', 'role.id', '=', 'role_permission.role_id')
            ->where('role.name', '=', 'guest')->pluck('path')->toArray();
        return $items;
    }

    private function getDefaultPermission() {
        $items = Db::table('acl_resource AS resource')->select(DB::raw('CONCAT(resource.type, "::", resource.path) AS `path`'))
            ->leftJoin('acl_role_permission AS role_permission', 'role_permission.resource_id', '=', 'resource.id')
            ->leftJoin('acl_role AS role', 'role.id', '=', 'role_permission.role_id')
            ->leftJoin('users AS user', 'user.role', '=', 'role.id')
            ->where('user.id', '=', 1)->pluck('path')->toArray();
        return $items;
    }

    private function checkPermission($slug) {
        if(!is_array($slug)) $slug = [$slug];
        $permissions = $this->getDefaultPermission();
        $access = array_intersect(array_map('strtolower', $permissions), array_map('strtolower', $slug));
        return $access;
    }
}
