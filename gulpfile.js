var gulp = require('gulp'),
    less = require("gulp-less"),
    cssmin = require("gulp-cssmin"),
    concat = require('gulp-concat'),
    shell = require("gulp-shell");
    // gulp_clean = require('gulp-clean'),
    // configuration = require('./resources/build/configuration');

// var options = configuration(elixir);

gulp.task('default', ['copy:folders', 'uikit-less', 'style-admin', 'uikit-js', 'app-vue'], function() {
    //node build/less
});

gulp.task('style', ['uikit-less', 'style-admin'], function() {
    //node build/less
});

// компилирование UIkit стилей
gulp.task('uikit-less', function() {
    return gulp.src('').pipe(shell(['cd resources/src/lib/uikit && node build/less && cd ../../../..']));
});

// компилирование UIkit скриптов
gulp.task('uikit-js', function() {
    return gulp.src('').pipe(shell(['cd resources/src/lib/uikit && node build/build']));
});

// копирование папки с шрифтами
gulp.task('copy:folders', function() {
    gulp.src('./resources/assets/fonts/**/*').pipe(gulp.dest('./public/fonts'));
    gulp.src('./resources/assets/img/**/*').pipe(gulp.dest('./public/img'));
    // gulp.src('./resources/assets/fonts/**/*.{ttf,woff,eof,svg}').pipe(gulp.dest('./public/fonts'));
});

// компилирование less + minify + concat
gulp.task('style-admin', function () {
    gulp.src([
        './resources/assets/style/uikit.min.css',
        './resources/assets/style/admin-ui.less'
    ]).pipe(less().on('error', function(err) {
        console.log(err);
    })).pipe(cssmin().on('error', function(err) {
        console.log(err);
    })).pipe(concat('ui.min.css', function(err) {
        console.log(err);
    })).pipe(gulp.dest('./public/style'));
});

// сборка Vue приложения
gulp.task('app-vue', function() {
    return gulp.src('').pipe(shell(['cd resources/src/webpack && node build/build.js']));
});

gulp.task("generateRoutes", function() {
    return gulp.src("").pipe( shell( [
        "php artisan laroute:generate",
    ]));
});

gulp.task("webpackApp", function() {
    return gulp.src("").pipe( shell( [
        "npm run uikit-less && npm run build",
    ]));
});

gulp.task("default1", ["generateRoutes", "webpackApp"], function() {
    elixir(function (mix) {
      mix.copy('resources/assets/js/routes.js', 'public/js/routes.js');

      mix.copy('resources/assets/images', options.directories.buildOutput + 'images');
      mix.copy('resources/assets/fonts', options.directories.buildOutput + 'fonts');

      mix.copy('public/assets/app/verify.js', 'public/js/verify.js');
      mix.copy('public/assets/style/ui.css', 'public/css/ui.css');
      mix.copy('public/assets/style/uikit-custome.css', 'public/css/uikit-custome.css');

      compileStyles(mix, options);
      compileScripts(mix, options);

      mix.version([
        'css/application.css',
        'css/ui.min.css',
        'css/uikit-custome.min.css',
        'css/verify.css',
        'js/verify-manifest.js',
        'js/verify.js',
        'js/application.js',
        'js/backbone-app.js',
        'js/routes.js'
      ], options.directories.buildOutput).task('clean:tmp');


      //----------------
      // Switches on auto-reloading of browser
      // mix.browserSync({
      //     proxy: 'localhost:8000'
      // });
    });

});

function compileStyles(mix, options) {
  "use strict";

  // SmartAdmin
  mix.sass([
    options.directories.smartAdminInput + '/sass/smartadmin-production-plugins.scss',
    options.directories.smartAdminInput + '/sass/smartadmin-production.scss',
    options.directories.smartAdminInput + '/sass/smartadmin-skins.scss'
  ], options.directories.tmpOutput + '/smartadmin.css');

mix.styles([
    options.directories.tmpInput + '/bootstrap.css',
    options.directories.smartAdminInput + '/js/plugin/chosen/chosen.css',
    options.directories.tmpInput + '/app.css'
  ], 'public/css/application.css');
}

function compileScripts(mix, options) {
  "use strict";

  compileSmartadminScripts(mix, options);

  // js-code for specific sections
  mix.babel([
    'sections/role.js',
    'sections/permissions.js'
  ], options.directories.tmpOutput + '/sections.js');

  //----------------
  // combine all in one and make version
mix.scripts([
    options.directories.nodeInput + '/moment/moment.js',
    options.directories.nodeInput + '/daterangepicker/daterangepicker.js'
    ], 'public/js/application.js');
}

function compileSmartadminScripts(mix, options) {
    "use strict";

    // Smart Admin Theme
    var smartAdminJSFiles = [
        // JQUERY SELECT2 INPUT
        '/js/plugin/select2/select2.js',
        // Full Calendar
        '/js/plugin/moment/moment.js'
    ];

    return mix.scripts(smartAdminJSFiles.map(function (file) {
        return options.directories.smartAdminInput + file;
    }), options.directories.tmpOutput + '/smartadmin.js');
}
