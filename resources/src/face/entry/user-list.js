"use strict";

import Vue from 'vue';
import VueRouter from 'vue-router';
import config from '@/core/config.js';
import UserListLayout from '@/layouts/user-list-layout.js';

Vue.use(VueRouter);

const router = new VueRouter({
    mode: 'history'
});

new Vue({
    el: "#root",
    // компоненты
    components: {
        UserListLayout
    },
    router
});
