import Vue from 'vue';
import UIkit from '@lib/uikit/dist/js/uikit.min.js';
import api from '@/core/api.js';
import pagination from '@/components/pagination/pagination.vue';
import ModalUserProfile from "@/components/template/modal.user.profile.vue";

export default {

    created: function() {
        this.getData();
    },

    // Вызывается сразу после того как экземпляр был смонтирован
    mounted: function() {
        // console.log(this);
    },

    // реактивные данные
    data: function() {
        return {
            searchText: '',
            // список пользователей
            userlist: {
                data: []
            },
            // состояние приложения
            state: {
                isLoading: false,
                // динамический вид модального окна
                modalView: null,
                // данные для модального окна
                modalData: {}
            }
        }
    },

    // компоненты
    components: {
        pagination
    },

    // Методы, которые будут подмешаны к экземпляру Vue
    methods: {
        getData: function(param) {
            let $this = this;
            let data = param || (this.$route.query.page ? {page: this.$route.query.page} : {});
            return api.get('/api/user/userlist', data, {notifyError: true}).then(function({data: response}) {
                $this.userlist = response.data;
                return Promise.resolve(response.data);
            }, function({data: response, request, config}) {
                return Promise.reject(response);
            });
        },

        onPageChange(data) {
            this.getData({page: data.value}).then(function(response) {
                history.pushState({}, '', location.origin + '?page=' + data.value);
            });
        },

        onModalShow: function(data) {
            let $this = this;
            data.type = data.type || "default";
            switch (data.type) {
                case "user.profile":
                    this.state.modalData = data;
                    this.state.modalView = ModalUserProfile;
                break;

                default:
                    UIkit.notification(`Modal type ${data.type} not found`, {status: 'warning'});
                break;
            }

            let modal = UIkit.modal("#modal-layout").show();
            $("#modal-layout").off('hidden').on('hidden', function(event) {
                $this.state.modalData = {};
            });
        }
    },

    // вычисляемые свойства
    computed: {
        filteredUsers: function () {
            let $this = this;
            let regexp = new RegExp(this.searchText, 'i');
            return this.userlist.data.filter(function(user) {
                return regexp.test(user.first_name) || regexp.test(user.last_name) || regexp.test(user.email);
            });
        }
    }
}
