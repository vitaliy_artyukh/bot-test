import Vue from 'vue';
import UIkit from "@lib/uikit/dist/js/uikit.min.js";

// export const ERR = "error";
// export const CODE = "code";
// export const MSG = "message";
// export const DATA = "data";

Vue.config.debug = true;
// должен ли Vue позволять Vue-devtools проводить инспекцию
Vue.config.devtools = true;
// Установите в false, чтобы отключить предупреждение о работе в режиме разработки при запуске Vue
Vue.config.productionTip = false;
// Устанавливает обработчик для ошибок, не пойманных во время рендеринга компонентов и в наблюдателях. Обработчик получит в параметрах ошибку и действующий экземпляр Vue.
Vue.config.errorHandler = function (err, vm, info) {
    // обработка ошибки
    // `info` это информация Vue-специфичной ошибки, например в каком хуке жизненного цикла
    // была найдена ошибка. Доступно только в версиях 2.2.0+
    console.error(err);
    console.log(vm);
    console.log(info);
}
// Назначает пользовательский обработчик предупреждений Vue во время выполнения
Vue.config.warnHandler = function (msg, vm, trace) {
    console.log(msg);
    console.log(trace);
  // `trace` — это трассировка иерархии компонентов
}

// notification
UIkit.components.notification.options.defaults.timeout = 8000;
UIkit.components.notification.options.defaults.pos = "top-right";

// tooltip
UIkit.components.tooltip.options.defaults.animation = "uk-animation-slide-top";

// modal
UIkit.components.modal.options.defaults.stack = true;
UIkit.components.modal.options.defaults.center = true;

export default {

}
