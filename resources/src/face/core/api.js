import config from "@/core/config.js";
import axios from "axios";
import UIkit from "@lib/uikit/dist/js/uikit.min.js";
import {has} from "lodash";

// allow use http client without Vue instance
export const http = axios.create({
    // baseURL: '/api/',
    headers: {
        "X-CSRF-TOKEN": window.app.csrf,
        "X-Requested-With": "XMLHttpRequest"
    }
});

http.interceptors.request.use(function(request) {
    if(!request.url) UIkit.notification("Request not have url", {status: "danger"});
    if(request.state) request.state.isLoading = true;
    return request;
});

http.interceptors.response.use(function(response) {
    if(response.config.state) response.config.state.isLoading = false;
    return response;
});

/**
* параметры callback для всех типов запросов
* @param config['notify'] - вывод уведомление [boolean]
* @param config['notifyError'] - уведомление об ошибке [boolean]
**/

const responseCallback = function(response, url, data, config) {
    if(config && config.notify === true) {
        UIkit.notification(config && config.notifyMessage ? config.notifyMessage : response.data.message, {status: "success"});
    }
};

const responseException = function(error, url, data, config) {
    if(error.response.config.notifyError === false) {
        //
    } else {
        if(_.has(error.response.data, "code") && error.response.data.code === 1400) {
            _.flatMap(error.response.data.data, function(n) { return n; }).map(function(msg) {
                UIkit.notification(msg, {status: "warning"});
            });
        } else {
            UIkit.notification(error.response.data.message || 'Error', {status: "warning"});
        }
    }
};

const api = {

    /**
    * @params
    * url: адрес запроса
    * data: данные
    * config: параметры запроса
    * @return
    * Promise success(response)
    * Promise error(error)
    **/

    // запрос типа GET
    get: function(url, data, config = {}) {
        config.params = data || null;
        config.onUploadProgress = function(progressEvent) {
            let percentCompleted = Math.round( (progressEvent.loaded * 100) / progressEvent.total );
            // console.log(percentCompleted);
        }
        config.onDownloadProgress = function(progressEvent) {
            //
        }
        return http.get(url, config).then(function(response) {
            responseCallback(response, url, data, config);
            return Promise.resolve(response);
        }).catch(function(error) {
            responseException(error, url, data, config);
            return Promise.reject(error.response);
        });
    },

    // запрос типа POST
    post: function(url, data, config) {
        return http.post(url, data, config).then(function(response) {
            responseCallback(response, url, data, config);
            return Promise.resolve(response);
        }).catch(function(error) {
            responseException(error, url, data, config);
            return Promise.reject(error.response);
        });
    },

    // запрос типа PUT
    put: function(url, data, config) {
        return http.put(url, data, config).then(function(response) {
            responseCallback(response, url, data, config);
            return Promise.resolve(response);
        }).catch(function(error) {
            responseException(error, url, data, config);
            return Promise.reject(error.response);
        });
    },

    // запрос типа DELETE
    delete: function(url, data, config = {}) {
        config.data = data;
        return http.delete(url, config).then(function(response) {
            responseCallback(response, url, data, config);
            return Promise.resolve(response);
        }).catch(function(error) {
            responseException(error, url, data, config);
            return Promise.reject(error.response);
        });
    }
};

export default api;
