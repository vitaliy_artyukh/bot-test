@extends('face.layouts.simple')

@section('content')
<div inline-template is="user-list-layout">
<div class="uk-margin-top uk-margin-bottom">
    <div class="uk-grid uk-margin">
        <div class="uk-width-expand@m">
            <h2>User list<span class="tm-table-counter uk-badge" v-text="userlist.total"></span></h2>
        </div>
        <div class="uk-width-auto@m uk-text-right">
            <form class="uk-form-ico uk-form-icon-flip uk-search uk-search-navbar">
                <i class="icon-search4"></i>
                <input class="uk-search-input" type="search" placeholder="search..." v-model="searchText">
            </form>
            <div class="uk-button-group">
                <a href="#" class="uk-button uk-button-primary" @click.prevent="onModalShow({type: 'user.profile', data: {first_name: '', last_name: ''}})"><i class="icon-plus3 uk-margin-small-right"></i>Add new</a>
            </div>
        </div>
    </div>

    <div class="uk-card uk-card-default">
        <table class="uk-table uk-table-hover uk-table-awesome tm-table-user">
            <thead class="uk-table-head">
                <tr class="uk-table-caption-subtitle">
                    <th class="uk-table-shrink">ID</th>
                    <th class="uk-table-shrink"></th>
                    <th>User name</th>
                    <th>Email</th>
                    <th>Phone</th>
                    <th width="126"></th>
                </tr>
            </thead>
            <tbody>
                <template v-if="filteredUsers.length">
                    <tr v-for="user in filteredUsers" :key="user.id">
                        <td><span class="tm-id" v-text="user.id"></span></td>
                        <td><span class="tm-avatar"><img :src="user.avatar" :alt="user.first_name"></span></td>
                        <td>
                            <div class="tm-name" v-cloak><span v-text="user.first_name"></span> <span v-text="user.last_name"></span></div>
                            <div class="tm-login" v-text="user.username"></div>
                        </td>
                        <td><div class="tm-email"><a :href="'mailto:' + user.email" v-text="user.email"></a></div></td>
                        <td><div class="tm-phone"><a :href="'tel:' + user.phone" v-text="user.phone"></a></div></td>
                        <td><a href="#" class="uk-button uk-button-default" @click.prevent="onModalShow({type: 'user.profile', data: user})"><i class="icon-pencil uk-margin-mini-right"></i>edit profile</a></td>
                    </tr>
                </template>
                <tr v-else>
                    <td colspan="10">Users not found</td>
                </tr>
            </tbody>
        </table>

        <pagination class="uk-flex-center" :items="userlist.total" :items-on-page="userlist.per_page" :current-page="userlist.current_page" @change="onPageChange"></pagination>
    </div>

    <div class="uk-flex-top" id="modal-layout" uk-modal="bg-close: false">
        <component :is="state.modalView" :data="state.modalData"></component>
    </div>
</div>
</div>
@endsection

@section('js')
<script type="text/javascript" src="/js/user-list.js"></script>
@endsection
