<div class="uk-container uk-container-expand uk-container-collapse tm-container-top">
    <nav class="uk-navbar-container uk-container tm-navbar-top" uk-navbar="mode: click; align: right">
        <div class="uk-navbar-left tm-logo-side">
            <a href="{{ route('userList') }}" class="uk-position-middle-left tm-base-logo">
                <img src="/img/logo.png" alt="Vintage">
            </a>
        </div>
        <div class="uk-navbar-item">
            <!-- <form class="uk-form-ico uk-form-icon-flip uk-search uk-search-navbar">
                <i class="icon-search4"></i>
                <input class="uk-search-input" type="search" placeholder="поиск..." v-model="searchText">
            </form> -->
        </div>
        <div class="uk-navbar-right">
            <!-- <ul class="uk-navbar-nav">
                <li><a href="#"><i class="icon-cog"></i></a></li>
                <li><a href="#"><i class="icon-bell3"></i></a></li>
                <li>
                    <a href="#">Виталий Артюх<i class="icon-arrow-down22 uk-margin-mini-left uk-margin-mini-right"></i><span><img src="/img/icon/icon-user-32.png" alt="user avatar"></span></a>
                    <div class="uk-navbar-dropdown">
                        <ul class="uk-nav uk-navbar-dropdown-nav">
                            <li class="uk-active"><a href="#">Active</a></li>
                            <li><a href="#">Item</a></li>
                            <li><a href="#">Item</a></li>
                        </ul>
                    </div>
                </li>
            </ul> -->
        </div>
    </nav>
</div>
