<div class="uk-grid uk-grid-collapse tm-footer" uk-grid="">
    <div class="uk-width-1-1@m">
        <div class="tm-content-wrap">
            <div class="uk-text-center uk-text-muted">powered by Laravel v.5.5  •  2017  •  made with ❤</div>
        </div>
    </div>
</div>
