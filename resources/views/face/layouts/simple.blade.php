<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width,initial-scale=1.0">
    <title>Bot - user</title>
    <link rel="stylesheet" href="/style/ui.min.css">
</head>
<body>
    <div id="root">
        @include('face.components.navbar-top')
        <div class="uk-container">
            @yield('content')
        </div>
        @include('face.components.footer')
    </div>
    <script type="text/javascript" src="/js/manifest.js"></script>
    <script type="text/javascript" src="/js/vendor.js"></script>
    <script>
        window.app = {!! json_encode([
            'csrf' => csrf_token()
        ]) !!};
    </script>
    @yield('js')
</body>
</html>
